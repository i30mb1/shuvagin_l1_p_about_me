package n7.test

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_SHORT
import n7.test.databinding.ActivityMainBinding
import java.util.*


class MainActivity : AppCompatActivity(), TextToSpeech.OnInitListener {

    lateinit var view: ActivityMainBinding
    lateinit var tts: TextToSpeech
    lateinit var defaultSharedPreferences: SharedPreferences
    val buttonPlayerIsPlaying = ObservableBoolean(false)

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            val result = tts.setLanguage(Locale.getDefault())
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Snackbar.make(view.root, getString(R.string.all_language_not_supported), LENGTH_SHORT).show()
            } else {
                view.bActivityMainStart.isEnabled = true
            }
        } else {
            Snackbar.make(view.root, getString(R.string.all_ooops), LENGTH_SHORT).show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initSharedPreferences()
        initViews()
        initTts()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        when (newConfig.orientation) {
            Configuration.ORIENTATION_PORTRAIT -> {
                view.ivActivityMainPhoto.visibility = View.VISIBLE
            }
            Configuration.ORIENTATION_LANDSCAPE -> {
                view.ivActivityMainPhoto.visibility = View.GONE
            }
        }
    }

    private fun initViews() {
        view = DataBindingUtil.setContentView(this, R.layout.activity_main)
        view.tvActivityMainBio.text = getBio()
        view.activity = this
    }

    private fun initSharedPreferences() {
        defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

    }

    private fun initTts() {
        tts = TextToSpeech(this, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_activity_main_share -> {
                implicitIntentShare()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun implicitIntentShare() {
        val textBio = getBio()
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_TEXT, textBio)
        }
        if (intent.resolveActivity(packageManager) != null) startActivity(
            Intent.createChooser(intent, getString(R.string.activity_main_share_via))
        ) else {
            Snackbar.make(view.root, getString(R.string.all_ooops), LENGTH_SHORT).show()
        }
    }


    fun speakOut(gg: View) {
        val text = getBio()
        if (TextUtils.isEmpty(text)) {
            Snackbar.make(view.root, getString(R.string.all_have_not_typed_text), LENGTH_SHORT).show()
        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, "1")
            tts.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                override fun onDone(utteranceId: String?) {
                    buttonPlayerIsPlaying.set(false)
                }

                override fun onError(utteranceId: String?) {
                    buttonPlayerIsPlaying.set(false)
                }

                override fun onStart(utteranceId: String?) {
                    buttonPlayerIsPlaying.set(true)
                }
            })
        }
    }

    fun speakStop(gg: View?) {
        buttonPlayerIsPlaying.set(false)
        if (tts.isSpeaking) {
            tts.stop()
        }
    }

    override fun onPause() {
        super.onPause()
        speakStop(null)
    }

    fun speakIn() {
        speakStop(null)
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
            putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.all_speak_please))
        }

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Snackbar.make(view.root, getString(R.string.speech_not_supported), LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    val stringArrayListExtra = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    view.tvActivityMainBio.text = stringArrayListExtra[0]
                    defaultSharedPreferences.edit {
                        putString(getString(R.string.key_bio), stringArrayListExtra[0])
                    }
                }
            }
        }
    }

    private fun getBio(): String {
        var text = defaultSharedPreferences.getString(getString(R.string.key_bio), "")
        if (TextUtils.isEmpty(text)) {
            text = resources.getText(R.string.activity_main_bio).toString()
        }
        return text!!
    }

    companion object {
        const val REQ_CODE_SPEECH_INPUT = 12
    }
}
