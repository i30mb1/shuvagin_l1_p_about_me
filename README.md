# FIRST APP
Min API level is set to [`21`](https://android-arsenal.com/api?level=21)
## Tech-stack
* [TextToSpeach](https://developer.android.com/reference/android/speech/tts/TextToSpeech) - Synthesizes speech from text for immediate playback or to create a sound file.

![your_image_name](scr.jpg = 200x200)